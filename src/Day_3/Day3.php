<?php

declare(strict_types=1);

namespace AdventOfCode\Day_3;

use AdventOfCode\Task;

class Day3 extends Task
{
    public function part1(): int
    {
        $total = 0;
        $priorities = $this->getPriorities();
        $backpacksContent = $this->loadFile();

        foreach ($backpacksContent as $backpackContent)
        {
            $length = strlen($backpackContent);
            $part1 = substr($backpackContent, 0, $length / 2);
            $part2 = substr($backpackContent, $length / 2);

            $part1 = str_split($part1);
            $part2 = str_split($part2);

            $common = array_intersect($part1, $part2);
            $common = array_values($common)[0];

            $total += $priorities[$common];
        }

        return $total;
    }

    public function part2(): int
    {
        $backpacksContent = $this->loadFile();
        $priorities = $this->getPriorities();
        $total = 0;

        $groups = array_chunk($backpacksContent, 3);


        foreach ($groups as $group) {
            $backpacks = [];
            foreach ($group as $elf) {
                $backpacks[] = str_split($elf);
            }
            $common = array_intersect(...$backpacks);
            $common = array_values($common)[0];
            $total += $priorities[$common];
        }

        return $total;
    }

    private function getPriorities(): array
    {
        $alphabet = array_merge(range('a', 'z'), range('A', 'Z'));
        $alphabet = array_flip($alphabet);

        array_walk($alphabet, fn(&$position) => $position += 1);

        return $alphabet;
    }
}

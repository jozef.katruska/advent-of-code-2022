<?php

use AdventOfCode\ExecuteTaskCommand;
use Symfony\Component\Console\Application;

require 'vendor/autoload.php';

$application = new Application();

$application->add(new ExecuteTaskCommand());

$application->run();
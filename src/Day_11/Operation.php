<?php

namespace AdventOfCode\Day_11;

enum Operation
{
    case ADDITION;
    case MULTIPLICATION;
}

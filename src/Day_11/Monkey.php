<?php

declare(strict_types=1);

namespace AdventOfCode\Day_11;

class Monkey
{
    private int $inspectedItems = 0;

    public function __construct(
        /** @var Item[] */
        public array $items,
        public readonly int $monkeyWhenTestPasses,
        public readonly int $monkeyWhenTestFails,
        private readonly Operation $operation,
        private readonly int|string $operationValue,
        public readonly int $testValue,
    ) {
    }

    public function throw(Item $item, Monkey $monkey): void
    {
        $key = array_search($item, $this->items);
        unset($this->items[$key]);
        $monkey->accept($item);
    }

    public function accept(Item $item): void
    {
        $this->items[] = $item;
    }

    public function test(Item $item): bool
    {
        return $item->getWorryLevel() % $this->testValue === 0;
    }

    public function inspect(Item $item): void
    {
        $value = $this->operationValue;
        if ($value === 'old') {
            $value = $item->getWorryLevel();
        }

        switch ($this->operation) {
            case Operation::ADDITION:
                $item->addWorry($value);
                break;
            case Operation::MULTIPLICATION:
                $item->multiplyWorry($value);
                break;
        }
        $this->inspectedItems++;
    }

    public function getInspectedItems(): int
    {
        return $this->inspectedItems;
    }
}

<?php

declare(strict_types=1);

namespace AdventOfCode\Day_9;

use AdventOfCode\Task;

class Day9 extends Task
{
    private array $knotHistory;

    private array $knotsPosition;

    private int $size;

    public function part1(): string|int
    {
        $this->size = 2;
        $this->init();

        foreach ($this->loadFile() as $row) {
            [$direction, $count] = explode(' ', $row);
            for ($i = 0; $i < $count; $i++) {
                switch ($direction) {
                    case 'R':
                        $this->knotsPosition[0][1]++;
                        break;
                    case 'L':
                        $this->knotsPosition[0][1]--;
                        break;
                    case 'U':
                        $this->knotsPosition[0][0]--;
                        break;
                    case 'D':
                        $this->knotsPosition[0][0]++;
                        break;
                }
                if (!$this->areKnotsTouching(0,1)) {
                    $this->moveKnot(0, 1);
                }

                foreach ($this->knotsPosition as $order => $knotPosition) {
                    $this->knotHistory[$order][$knotPosition[0]][$knotPosition[1]] = 1;
                }
            }
        }

        return $this->getVisitedSquaresForKnot(1);
    }

    public function part2(): string|int
    {
        unset($this->knotsPosition, $this->knotHistory);
        $this->size = 10;
        $this->init();

        foreach ($this->loadFile() as $row) {
            [$direction, $count] = explode(' ', $row);
            for ($i = 0; $i < $count; $i++) {
                switch ($direction) {
                    case 'R':
                        $this->knotsPosition[0][1]++;
                        break;
                    case 'L':
                        $this->knotsPosition[0][1]--;
                        break;
                    case 'U':
                        $this->knotsPosition[0][0]--;
                        break;
                    case 'D':
                        $this->knotsPosition[0][0]++;
                        break;
                }
                for ($j = 0; $j < $this->size - 1; $j++) {
                    if (!$this->areKnotsTouching($j,$j + 1)) {
                        $this->moveKnot($j, $j + 1);
                    }
                }


                foreach ($this->knotsPosition as $order => $knotPosition) {
                    $this->knotHistory[$order][$knotPosition[0]][$knotPosition[1]] = 1;
                }
            }
        }

        return $this->getVisitedSquaresForKnot(9);
    }

    private function init(): void
    {
        for ($i = 0; $i < $this->size; $i++) {
            $this->knotHistory[$i][0][0] = 1;
            $this->knotsPosition[$i] = [0, 0];
        }
    }

    private function areKnotsTouching(int $knotToCompareOrder, int $knotOrder): bool
    {
        if ($this->knotsPosition[$knotToCompareOrder] === $this->knotsPosition[$knotOrder]) {
            return true;
        }

        if (
            abs($this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0]) < 2 &&
            abs($this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1]) < 2
        ) {
            return true;
        }

        return false;
    }

    private function moveKnot(int $knotToCompareOrder, int $knotOrder): void
    {
        if (
            $this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0] === 0 &&
            abs($this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1]) <= 2
        ) {
            $this->knotsPosition[$knotOrder][1] += $this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1] > 0 ? 1 : -1;
        } elseif (
            $this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1] === 0 &&
            abs($this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0]) <= 2
        ) {
            $this->knotsPosition[$knotOrder][0] += $this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0] > 0 ? 1 : -1;
        } elseif (
            abs($this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0]) >= 2
        ) {
            $this->knotsPosition[$knotOrder][0] += $this->knotsPosition[$knotToCompareOrder][0] - $this->knotsPosition[$knotOrder][0] > 0 ? 1 : -1;
            $this->knotsPosition[$knotOrder][1] = $this->knotsPosition[$knotToCompareOrder][1];
        } elseif (abs($this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1]) >= 2)  {
            $this->knotsPosition[$knotOrder][1] += $this->knotsPosition[$knotToCompareOrder][1] - $this->knotsPosition[$knotOrder][1] > 0 ? 1 : -1;
            $this->knotsPosition[$knotOrder][0] = $this->knotsPosition[$knotToCompareOrder][0];
        }
    }

    private function getVisitedSquaresForKnot(int $knot): int
    {
        $visitedPositions = 0;

        foreach ($this->knotHistory[$knot] as $row) {
            $visitedPositions += array_sum($row);
        }

        return $visitedPositions;
    }

    private function print(int $order): void
    {
        $history = $this->knotHistory[$order];

        $out = [];

        foreach ($history as $row) {
            $out[] = implode(' ', $row);
        }

        echo implode("\n", $out);
    }
}

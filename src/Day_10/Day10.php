<?php

declare(strict_types=1);

namespace AdventOfCode\Day_10;

use AdventOfCode\Task;

class Day10 extends Task
{
    private int $cycle = 1;

    private int $x = 1;

    private array $values = [];

    private array $crtOutput = [];

    public function part1(): string|int
    {
        foreach ($this->loadFile() as $row) {
            if ($row === 'noop') {
                $this->increaseCycle();
            } elseif (str_starts_with($row, 'addx')) {
                [, $number] = explode(' ', $row);
                $number = intval($number);
                for ($i = 0; $i < 2; $i++) {
                    $this->increaseCycle();
                }
                $this->x += $number;
            }
        }

        return array_sum($this->values);
    }

    public function part2(): string|int
    {
        foreach ($this->loadFile() as $row) {
            if ($row === 'noop') {
                $this->increaseCycleForRender();
            } elseif (str_starts_with($row, 'addx')) {
                [, $number] = explode(' ', $row);
                $number = intval($number);
                for ($i = 0; $i < 2; $i++) {
                    $this->increaseCycleForRender();
                }
                $this->x += $number;
            }
        }

        $out = array_chunk($this->crtOutput, 40);

        foreach ($out as $row) {
            echo implode('', $row) . PHP_EOL;
        }

        return '';
    }

    private function increaseCycle(): void
    {
        if (in_array($this->cycle, [20, 60, 100, 140, 180, 220])) {
            $this->values[] = $this->cycle * $this->x;
        }

        $this->cycle++;
    }

    private function increaseCycleForRender(): void
    {
        var_dump($this->x - 1, $this->x + 2);
        $this->crtOutput[] = $this->cycle % 40 >= $this->x && $this->cycle % 40 <= $this->x + 2 ? '#' : '.';

        $this->cycle++;
    }
}

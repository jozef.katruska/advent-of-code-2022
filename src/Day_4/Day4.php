<?php

declare(strict_types=1);

namespace AdventOfCode\Day_4;

use AdventOfCode\Task;

class Day4 extends Task
{
    public function part1(): int
    {
        $total = 0;
        $pairs = $this->parsePairs();

        foreach ($pairs as $pair) {
            if ((count(array_intersect($pair[0], $pair[1])) === count($pair[1])) ||
                (count(array_intersect($pair[1], $pair[0])) === count($pair[0]))
            ) {
                $total++;
            }

        }

        return $total;
    }

    public function part2(): int
    {
        $total = 0;
        $pairs = $this->parsePairs();

        foreach ($pairs as $pair) {
            if (array_intersect($pair[0], $pair[1])) {
                $total++;
            }
        }

        return $total;
    }

    private function parsePairs(): array
    {
        $out = [];
        foreach ($this->loadFile() as $line) {
            $groups = explode(',', $line);
            $elf1 = $this->getAssignments($groups[0]);
            $elf2 = $this->getAssignments($groups[1]);
            $out[] = [$elf1, $elf2];
        }

        return $out;
    }

    private function getAssignments(string $range): array
    {
        $range = explode('-', $range);
        $start = (int) $range[0];
        $stop = (int) $range[1];

        return range($start, $stop);
    }
}

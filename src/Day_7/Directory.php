<?php

declare(strict_types=1);

namespace AdventOfCode\Day_7;

class Directory
{
    private array $content = [];

    public function __construct(public readonly string $name)
    {
    }

    public function add(File|Directory $content): void
    {
        $this->content[] = $content;
    }

    public function getSize(): int
    {
        $total = 0;

        foreach ($this->content as $item) {
            $total += $item instanceof Directory ? $item->getSize() : $item->size;
        }

        return $total;
    }

    public function getContent(): array
    {
        return $this->content;
    }
}

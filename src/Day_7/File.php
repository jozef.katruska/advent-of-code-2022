<?php

declare(strict_types=1);

namespace AdventOfCode\Day_7;

class File
{
    public function __construct(public readonly int $size, public readonly string $name)
    {
    }
}

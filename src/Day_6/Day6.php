<?php

declare(strict_types=1);

namespace AdventOfCode\Day_6;

use AdventOfCode\Task;

class Day6 extends Task
{
    public function part1(): string|int
    {
        return $this->getUniqueCharactersOffset(4);
    }

    public function part2(): string|int
    {
        return $this->getUniqueCharactersOffset(14);
    }

    private function getUniqueCharactersOffset(int $numberOfCharacters): int
    {
        $chars = str_split($this->loadFile()[0]);
        $characterCount = count($chars);

        for ($index = $numberOfCharacters - 1; $index < $characterCount; $index++) {
            $array = array_slice($chars, $index, $numberOfCharacters);
            $array = array_unique($array);
            if (count($array) === $numberOfCharacters) {
                return $index + $numberOfCharacters;
            }
        }

        return 0;
    }
}

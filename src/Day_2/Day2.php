<?php

declare(strict_types=1);

namespace AdventOfCode\Day_2;

use AdventOfCode\Task;

class Day2 extends Task
{
    private const OPPONENT_ROCK = 'A';
    private const OPPONENT_PAPER = 'B';
    private const OPPONENT_SCISSORS = 'C';

    private const YOU_ROCK = 'X';
    private const YOU_PAPER = 'Y';
    private const YOU_SCISSORS = 'Z';

    private const WIN_CONDITIONS = [
        self::OPPONENT_ROCK => self::YOU_PAPER,
        self::OPPONENT_PAPER => self::YOU_SCISSORS,
        self::OPPONENT_SCISSORS => self::YOU_ROCK,
    ];

    private const DRAW_CONDITIONS = [
        self::OPPONENT_ROCK => self::YOU_ROCK,
        self::OPPONENT_PAPER => self::YOU_PAPER,
        self::OPPONENT_SCISSORS => self::YOU_SCISSORS,
    ];

    private const LOOSE_CONDITIONS = [
        self::OPPONENT_ROCK => self::YOU_SCISSORS,
        self::OPPONENT_PAPER => self::YOU_ROCK,
        self::OPPONENT_SCISSORS => self::YOU_PAPER,
    ];

    private const SELECTION_POINTS = [
        self::YOU_ROCK => 1,
        self::YOU_PAPER => 2,
        self::YOU_SCISSORS => 3,
    ];

    public function part1(): int
    {
        $turns = $this->loadFile();
        $points = 0;

        foreach ($turns as $turn) {
            $turn = explode(' ', $turn);
            $opponent = $turn[0];
            $you = $turn[1];

            $points += $this->calculateRoundPoints($opponent, $you);
        }

        return $points;
    }

    public function part2(): int
    {
        $turns = $this->loadFile();
        $points = 0;

        foreach ($turns as $turn) {
            $turn = explode(' ', $turn);
            $opponent = $turn[0];
            $outcome = $turn[1];
            switch ($outcome) {
                case 'X':
                    $you = self::LOOSE_CONDITIONS[$opponent];
                    break;
                case 'Y':
                    $you = self::DRAW_CONDITIONS[$opponent];
                    break;
                case 'Z':
                    $you = self::WIN_CONDITIONS[$opponent];
                    break;
            }
            $points += $this->calculateRoundPoints($opponent, $you);
        }

        return $points;
    }

    private function isWin(string $opponent, string $you): bool
    {
        return self::WIN_CONDITIONS[$opponent] === $you;
    }

    private function isDraw(string $opponent, string $you): bool
    {
        return self::DRAW_CONDITIONS[$opponent] === $you;
    }

    private function calculateRoundPoints(string $opponent, string $you): int
    {
        $points = 0;
        $points += self::SELECTION_POINTS[$you];

        if ($this->isWin($opponent, $you)) {
            $points += 6;
        } elseif ($this->isDraw($opponent, $you)) {
            $points += 3;
        }

        return $points;
    }
}

<?php

declare(strict_types=1);

namespace AdventOfCode\Day_8;

use AdventOfCode\Task;

class Day8 extends Task
{
    public function part1(): string|int
    {
        $matrix = $this->buildMatrix();
        $visible = 0;

        $columnsLength = count($matrix) - 1;
        foreach ($matrix as $columnId => $row) {
            $rowLength = count($row) - 1;
            foreach ($row as $rowId => $height) {
                if ($this->isOnEdge($columnsLength, $rowLength, $columnId, $rowId)) {
                    $visible++;
                    continue;
                }
                $treesHeights = $this->getAdjacentTreesHeights($matrix, $rowId, $columnId, $row);
                if ($height > max($treesHeights['east']) ||
                    $height > max($treesHeights['west']) ||
                    $height > max($treesHeights['south']) ||
                    $height > max($treesHeights['north'])
                ) {
                    $visible++;
                }
            }
        }

        return $visible;
    }

    public function part2(): string|int
    {
        $matrix = $this->buildMatrix();
        $highestScenicScore = 0;

        $columnsLength = count($matrix) - 1;
        foreach ($matrix as $columnId => $row) {
            $rowLength = count($row) - 1;
            foreach ($row as $rowId => $height) {
                if ($this->isOnEdge($columnsLength, $rowLength, $columnId, $rowId)) {
                    continue;
                }
                $treesHeight = $this->getAdjacentTreesHeights($matrix, $rowId, $columnId, $row);
                $eastScore = $this->findBlockingTree($height, $treesHeight['east'], true);
                $westScore = $this->findBlockingTree($height, $treesHeight['west']);
                $southScore = $this->findBlockingTree($height, $treesHeight['south']);
                $northScore = $this->findBlockingTree($height, $treesHeight['north'], true);

                $scenicScore = $eastScore * $westScore * $southScore * $northScore;

                if ($scenicScore > $highestScenicScore) {
                    $highestScenicScore = $scenicScore;
                }
            }
        }

        return $highestScenicScore;
    }

    private function buildMatrix(): array
    {
        $matrix = [];

        foreach ($this->loadFile() as $row) {
            $row = array_map('intval', str_split($row));
            $matrix[] = $row;
        }

        return $matrix;
    }

    private function isOnEdge(int $columnLength, int $rowLength, int $xPosition, int $yPosition): bool
    {
        return $xPosition === 0 || $xPosition === $rowLength || $yPosition === 0 || $yPosition === $columnLength;
    }

    private function getAdjacentTreesHeights(array $matrix, int $xPosition, int $yPosition, array $row): array
    {
        $column = $this->getColumnHeights($matrix, $xPosition);

        return [
            'east' => array_slice($row, 0, $xPosition) + [0],
            'west' => array_slice($row, $xPosition + 1) + [0],
            'north' => array_slice($column, 0, $yPosition) + [0],
            'south' => array_slice($column, $yPosition + 1) + [0]
        ];
    }

    private function getColumnHeights(array $matrix, int $xPosition): array
    {
        $out = [];
        foreach ($matrix as $row) {
            $out[] = $row[$xPosition];
        }

        return $out;
    }

    private function findBlockingTree(int $currentHeight, array $treesHeight, bool $inverseDirection = false): int
    {
        if ($inverseDirection) {
            $treesHeight = array_reverse($treesHeight);
        }
        foreach ($treesHeight as $i => $treeHeight) {
            if ($treeHeight >= $currentHeight) {
                return $i + 1;
            }
        }

        return count($treesHeight);
    }
}

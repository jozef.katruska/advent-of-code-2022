<?php

declare(strict_types=1);

namespace AdventOfCode\Day_11;

class Item
{
    public function __construct(private int $worryLevel)
    {
    }

    public function addWorry(int $level): void
    {
        $this->worryLevel += $level;
    }

    public function multiplyWorry(int $level): void
    {
        $this->worryLevel *= $level;
    }

    public function getWorryLevel(): int
    {
        return $this->worryLevel;
    }

    public function relief(?int $stressReliveDivider): void
    {
        $this->worryLevel = $stressReliveDivider ? $this->worryLevel % $stressReliveDivider: (int) floor($this->worryLevel / 3) ;
    }
}

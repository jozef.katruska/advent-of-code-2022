<?php

namespace AdventOfCode\Day_1;

use AdventOfCode\Task;

class Day1 extends Task
{
    public function part1(): int
    {
        $calories = $this->getCaloriesPerElf();

        return max($calories);
    }

    public function part2(): int
    {
        $calories = $this->getCaloriesPerElf();

        rsort($calories, SORT_NUMERIC);

        return $calories[0] + $calories[1] + $calories[2];
    }

    private function getCaloriesPerElf(): array
    {
        $calories = $this->loadFile();
        $total = [];
        $elfCalorie = 0;

        foreach ($calories as $calory) {
            $calory = (int) $calory;
            $elfCalorie += $calory;

            if (empty($calory)) {
                $total[] = $elfCalorie;
                $elfCalorie = 0;
            }
        }

        return $total;
    }
}
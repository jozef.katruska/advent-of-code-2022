<?php

declare(strict_types=1);

namespace AdventOfCode\Day_7;

use Exception;

class FileSystem
{
    private string $currentDirectory = '/';

    private array $contents = [];

    public function __construct()
    {
        $this->contents[] = new Directory('/');
    }

    /**
     * adds item to current directory
     */
    public function add(Directory|File $item): void
    {
        $explodedPath = $this->formatPath($this->currentDirectory);
        $directory = $this->get($this->contents, $explodedPath);
        $directory->add($item);
    }

    public function moveTo(string $path): void
    {
        if (!str_starts_with($path, '/')) {
            $path = rtrim($this->currentDirectory, '/') . '/' . $path;
        }
        $explodedPath = $this->formatPath($path);
        $this->get($this->contents, $explodedPath);

        $this->currentDirectory = $path;
    }

    public function moveUp(): void
    {
        $path = explode('/', $this->currentDirectory);
        array_pop($path);

        $this->currentDirectory = implode('/', $path);
    }

    public function getContents(): array
    {
        return $this->contents;
    }

    private function get(array $array, array $path): Directory
    {
        $dirName = array_shift($path);

        foreach ($array as $item) {
            if (!$item instanceof Directory) {
                continue;
            }
            if ($item->name === $dirName && empty($path)) {
                return $item;
            } elseif ($item->name === $dirName) {
                return $this->get($item->getContent(), $path);
            }
        }

        throw new Exception(sprintf('Dir %s doesn\'t exists', implode('/', $path)));
    }

    private function formatPath(string $path): array
    {
        $explodedPath = $path === '/' ? ['/'] : explode('/', $path);
        return ['/'] + array_filter($explodedPath);
    }
}

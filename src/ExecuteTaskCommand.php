<?php

declare(strict_types=1);

namespace AdventOfCode;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:task')]
class ExecuteTaskCommand extends Command
{
    private const ARGUMENT_DAY = 'day';
    private const ARGUMENT_PART = 'part';

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_DAY, InputArgument::REQUIRED);
        $this->addArgument(self::ARGUMENT_PART, InputArgument::OPTIONAL|InputArgument::IS_ARRAY);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $day = $input->getArgument(self::ARGUMENT_DAY);
        $parts = $input->getArgument(self::ARGUMENT_PART);

        if ($parts === []) {
            $parts = [1, 2];
        }

        $fqdn = sprintf('\\AdventOfCode\\Day_%s\\Day%s', $day, $day);
        $class = new $fqdn();

        assert($class instanceof Task);

        foreach ($parts as $part) {
            $method = sprintf('part%s', $part);
            $output->writeln(sprintf('<info>Part %s solution:</info>', $part));
            $output->writeln((string) $class->$method());
        }

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace AdventOfCode;

use ReflectionClass;

abstract class Task
{
    public abstract function part1(): string|int;

    public abstract function part2(): string|int;

    protected function loadFile(string $fileName = 'input'): array
    {
        $reflection = new ReflectionClass($this);
        $path = dirname($reflection->getFileName()) . '/' . $fileName;

        $file = file_get_contents($path);
        return explode("\n", $file);
    }
}

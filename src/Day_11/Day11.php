<?php

declare(strict_types=1);

namespace AdventOfCode\Day_11;

use AdventOfCode\Task;

class Day11 extends Task
{
    /** @var Monkey[] */
    private array $monkeys;

    public function part1(): string|int
    {
        $this->parseInput();

        for ($round = 0; $round < 20; $round++) {
            foreach ($this->monkeys as $monkey) {
                $this->processItems($monkey);
            }
        }

        $inspectedItems = [];

        foreach ($this->monkeys as $monkey) {
            $inspectedItems[] = $monkey->getInspectedItems();
        }

        rsort($inspectedItems);

        return $inspectedItems[0] * $inspectedItems[1];
    }

    public function part2(): string|int
    {
        unset($this->monkeys);
        $this->parseInput();

        $divider = 1;

        foreach ($this->monkeys as $monkey) {
            $divider *= $monkey->testValue;
        }

        for ($round = 0; $round < 10000; $round++) {
            foreach ($this->monkeys as $monkey) {
                $this->processItems($monkey, $divider);
            }
        }

        $inspectedItems = [];

        foreach ($this->monkeys as $monkey) {
            $inspectedItems[] = $monkey->getInspectedItems();
        }

        rsort($inspectedItems);

        return $inspectedItems[0] * $inspectedItems[1];
    }

    private function parseInput(): void
    {
        $monkeysData = array_chunk($this->loadFile(), 7);
        foreach ($monkeysData as $monkeyData) {
            $this->monkeys[] = $this->parseMonkeyData($monkeyData);
        }
    }

    private function parseMonkeyData(array $data): Monkey
    {
        array_walk($data, fn (string $row) => trim($row));
        return new Monkey(
            $this->parseStartingItems($data[1]),
            $this->parseTestResultValue($data[4]),
            $this->parseTestResultValue($data[5]),
            $this->parseOperation($data[2]),
            $this->parseOperationValue($data[2]),
            $this->parseTestValue($data[3])
        );
    }

    private function parseStartingItems(string $string): array
    {
        $items = [];

        preg_match('/Starting items: (.*)/', $string, $matches);
        $startingItems = explode(', ', $matches[1]);
        foreach ($startingItems as $startingItem) {
            $items[] = new Item((int) $startingItem);
        }

        return $items;
    }

    private function parseOperation(string $string): Operation
    {
        preg_match('/Operation: new = old (\*|\+)/', $string, $matches);

        return match ($matches[1]) {
            '*' => Operation::MULTIPLICATION,
            '+' => Operation::ADDITION
        };
    }

    private function parseOperationValue(string $string): int|string
    {
        preg_match('/Operation: new = old [\*|\+] (.*)/', $string, $matches);

        return is_numeric($matches[1]) ? intval($matches[1]) : $matches[1];
    }

    private function parseTestValue(string $string): int
    {
        preg_match('/Test: divisible by (.*)/', $string, $matches);

        return intval($matches[1]);
    }

    private function parseTestResultValue(string $string): int
    {
        preg_match('/If (true|false): throw to monkey (.*)/', $string, $matches);

        return intval($matches[2]);
    }

    private function processItems(Monkey $monkey, ?int $stressReliefDivider = null): void
    {
        foreach ($monkey->items as $item) {
            $monkey->inspect($item);
            $item->relief($stressReliefDivider);
            $recipientMonkey = $monkey->test($item) ? $monkey->monkeyWhenTestPasses : $monkey->monkeyWhenTestFails;
            $recipientMonkey = $this->monkeys[$recipientMonkey];
            $monkey->throw($item, $recipientMonkey);
        }
    }
}

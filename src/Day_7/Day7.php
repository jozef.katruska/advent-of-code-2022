<?php

declare(strict_types=1);

namespace AdventOfCode\Day_7;

use AdventOfCode\Task;

class Day7 extends Task
{
    private array $directorySizes = [];

    public function part1(): string|int
    {
        $fileSystem = $this->buildFileSystem();

        $this->getSize($fileSystem->getContents());

        return array_sum(array_filter($this->directorySizes, fn(int $total) => $total <= 100000));
    }

    public function part2(): string|int
    {
        $this->directorySizes = [];
        $totalSpace = 70000000;
        $fileSystem = $this->buildFileSystem();

        $usedSpace = $fileSystem->getContents()[0]->getSize();
        $freeSpace = $totalSpace - $usedSpace;
        $this->getSize($fileSystem->getContents());


        $this->directorySizes = array_filter($this->directorySizes, fn(int $size) => $size > (30000000 - $freeSpace));
        sort($this->directorySizes);

        return $this->directorySizes[0];
    }

    private function buildFileSystem(): FileSystem
    {
        $fileSystem = new FileSystem();

        foreach ($this->loadFile() as $row) {
            if ($row === '$ ls') {
                continue;
            } elseif (str_starts_with($row, '$ cd') && $this->getDestination($row) === '..') {
                $fileSystem->moveUp();
            } elseif (str_starts_with($row, '$ cd')) {
                $fileSystem->moveTo($this->getDestination($row));
            } elseif (str_starts_with($row, 'dir')) {
                $name = explode(' ', $row);
                $directory = new Directory($name[1]);
                $fileSystem->add($directory);
            } else {
                $fileInfo = explode(' ', $row);
                $file = new File((int) $fileInfo[0], $fileInfo[1]);
                $fileSystem->add($file);
            }
        }

        return $fileSystem;
    }

    private function getDestination(string $command): string
    {
        preg_match('/\$ cd (.*)/', $command, $matches);
        return $matches[1];
    }

    private function getSize(array $content): int
    {
        $total = 0;

        foreach ($content as $item) {
            if ($item instanceof File) {
                $total += $item->size;
            }
            if ($item instanceof Directory) {
                $total += $this->getSize($item->getContent());
            }
        }

        $this->directorySizes[] = $total;

        return $total;
    }
}

<?php

declare(strict_types=1);

namespace AdventOfCode\Day_5;

use AdventOfCode\Task;

class Day5 extends Task
{
    private array $stacks = [
        1 => ['L', 'N', 'W', 'T', 'D'],
        2 => ['C', 'P', 'H'],
        3 => ['W', 'P', 'H', 'N', 'D', 'G', 'M', 'J'],
        4 => ['C', 'W', 'S', 'N', 'T', 'Q', 'L'],
        5 => ['P', 'H', 'C', 'N'],
        6 => ['T', 'H', 'N', 'D', 'M', 'W', 'Q', 'B'],
        7 => ['M', 'B', 'R', 'J', 'G', 'S', 'L'],
        8 => ['Z', 'N', 'W', 'G', 'V', 'B', 'R', 'T'],
        9 => ['W', 'G', 'D', 'N', 'P', 'L'],
    ];

    public function part1(): string
    {
        $stacks = $this->stacks;

        foreach ($this->loadFile() as $row) {
            preg_match('/move (\d+) from (\d+) to (\d+)/', $row, $matches);
            [,$count, $from, $to] = $matches;

            for ($i = 0; $i < $count; $i++) {
                $item = array_pop($stacks[$from]);
                $stacks[$to][] = $item;
            }

            unset($matches);
        }

        $out = [];
        foreach ($stacks as $stack) {
            $out[] = $this->getLastItemFromArray($stack);
        }

        return implode('', $out);
    }

    public function part2(): string
    {
        $stacks = $this->stacks;

        foreach ($this->loadFile() as $row) {
            preg_match('/move (\d+) from (\d+) to (\d+)/', $row, $matches);
            [,$count, $from, $to] = $matches;
            $stacks[$to] = array_merge($stacks[$to], array_splice($stacks[$from], ($count *-1)));
            unset($matches);
        }

        $out = [];
        foreach ($stacks as $stack) {
            $out[] = $this->getLastItemFromArray($stack);
        }

        return implode('', $out);
    }

    private function getLastItemFromArray(array $array): string
    {
        return $array[array_key_last($array)];
    }
}
